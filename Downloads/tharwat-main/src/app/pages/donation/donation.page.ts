import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';
import { EnvService } from './../../services/env.service';
import{ HttpServiceService}  from './../../services/http-service.service'
import {Donations_info} from 'src/app/models/donations';
@Component({
  selector: 'app-donation',
  templateUrl: './donation.page.html',
  styleUrls: ['./donation.page.scss'],
})
export class DonationPage implements OnInit {
  donations_info: Donations_info;

  constructor(
  private alertService: AlertService,
  private env: EnvService,
  private httpService :HttpServiceService, ){ }

  ngOnInit() {
  }
  ionViewWillEnter() {
    // this.httpService.makeGet('auth/donations').subscribe(
    //   Donations_info => {
    //     this. Donations_info = Donations_info
    //     if(!this.Donations_info){
    //       this.Donations_info= {
    //         furniture :'',
    //  clothe :''
    //       }
    //     }
    //   },
    //   error => {
    //     console.log(error);
    //   },
    //   () => {
        
    //   }
    // );
  }

  

  donationModal() {

    let data = {
      donations_info: this.donations_info
    }
    console.log("dona data ")

    console.log(data)
    this.httpService.makePost(this.env.API_URL + 'auth/receive_donation', this.donations_info).subscribe(
      data => {
        this.alertService.presentToast("تم حفظ البيانات بنجاح");
      },
      error => {
        console.log(error);
      },
      () => {
        
      }
    )

}
}